require("dotenv").config();
const gulp = require("gulp"),
  path = require("path"),
  gulpShopify = require("gulp-shopify-upload-by-bitcode"),
  watch = require("gulp-watch"),
  concat = require("gulp-concat"),
  plumber = require("gulp-plumber"),
  sass = require("gulp-sass"),
  postcss = require("gulp-postcss"),
  rename = require("gulp-rename"),
  autoprefixer = require("autoprefixer"),
  sourcemaps = require("gulp-sourcemaps"),
  shopifyApi = require("shopify-api-node"),
  replace = require("gulp-replace"),
  // gulpif = require("gulp-if"),
  webpackStream = require("webpack-stream"),
  webpackConfig = require("./webpack.config.js"),
  imagemin = require("gulp-imagemin"),
  Axios = require("axios"),
  browsersync = require("browser-sync").create(),
  clearConsole = require("react-dev-utils/clearConsole");

/*Project information*/

const {
  APIKEY_PRIVATE_APP,
  PASSWORD_PRIVATE_APP,
  SITE_URL,
  THEME_ID,
  BROWSER_SYNC_PORT,
  ACCESS_DEST,
  NAME_FILE_MERGE_CSS,
  PATH_SSL_KEY,
  PATH_SSL_CRT
} = process.env;

/*Add source map to css and js file*/
const sourceMappingURLCSSregExp = new RegExp(
  "(.*?[/*]{2,}# sourceMappingURL=)(.*?)([/*]{2})",
  "g"
);
const sourceMappingURLCSSreplace =
  "{% raw %}$1{% endraw %}$2{% raw %}$3{% endraw %}";

/*********************
List gulp task
**********************/

gulp.task("mini-image", async () => {
  gulp
    .src([
      "src/assets/**/*.jpeg",
      "src/assets/**/*.gif",
      "src/assets/**/*.svg",
      "src/assets/**/*.png"
    ])
    .pipe(imagemin())
    .pipe(gulp.dest("src/assets"));
});

// gulp.task("deploy-production", async () => {
//   await gulp.start("mini-image");
// });

gulp.task("mergecss", async () => {
  gulp
    .src(["local/{scss,css-libs,styles}/theme.scss"])
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(replace(/({{|}}|{%|%})/g, "/*!$1*/"))
    .pipe(postcss([autoprefixer()]))
    .pipe(replace(/\/\*!({{|}}|{%|%})\*\//g, "$1"))
    .pipe(concat(NAME_FILE_MERGE_CSS))
    .pipe(
      sourcemaps.write(".", {
        sourceMappingURL: makeLiquidSourceMappingURL
      })
    )
    .pipe(rename(appendLiquidExt))
    .pipe(replace(sourceMappingURLCSSregExp, sourceMappingURLCSSreplace))
    .pipe(gulp.dest(ACCESS_DEST))
    .pipe(gulp.dest("./src/assets"));
});

gulp.task("reload-on-css", ["mergecss"]);

gulp.task("mergejs", async () => {
  gulp
    .src(["local/{js,js-libs}/main.js"])
    .pipe(webpackStream(webpackConfig))
    .pipe(rename(appendLiquidExt))
    .pipe(gulp.dest(ACCESS_DEST))
    .pipe(gulp.dest("./src/assets"));
});

gulp.task("reload-on-js", ["mergejs"]);

gulp.task("watch", function() {
  gulp.watch("local/{scss,css-libs,styles}/**/*.{css,less,scss,liquid}", [
    "reload-on-css"
  ]);
  gulp.watch("local/{js,js-libs}/**/*.{js,jsx}", ["reload-on-js"]);
});

gulp.task("shopifywatch", ["watchdelete"], () => {
  return watch(
    "src/+(assets|layout|config|sections|snippets|templates|locales)/**"
  ).pipe(
    gulpShopify(APIKEY_PRIVATE_APP, PASSWORD_PRIVATE_APP, SITE_URL, THEME_ID, {
      callback: reload
    })
  );
});

gulp.task("watchdelete", function() {
  gulp.watch(
    "src/+(assets|layout|config|sections|snippets|templates|locales)/**/*",
    res => {
      if (res.type === "deleted") {
        const key = makeAssetKey(res.path);
        console.log(
          "deletefile",
          `https://${SITE_URL}/admin/themes/#{${THEME_ID}}/assets.json?asset[key]=${key}`
        );
        const shopify = new shopifyApi({
          shopName: SITE_URL,
          apiKey: APIKEY_PRIVATE_APP,
          password: PASSWORD_PRIVATE_APP
        });
        const param = {
          asset: {
            key: key
          }
        };
        shopify.asset.delete(THEME_ID, param);
        // Axios.delete(
        //   `https://${SITE_URL}/admin/themes/#{${THEME_ID}}/assets.json?asset[key]=${key}`
        // );

        browsersync.reload();
      }
    }
  );
});

gulp.task("browsersync", function(done) {
  clearConsole();

  browsersync.init(
    {
      port: BROWSER_SYNC_PORT,
      ui: { port: Number(BROWSER_SYNC_PORT) + 1 },
      proxy: `https://${SITE_URL}`,
      browser: "google chrome",
      notify: true,
      startPath: `/?preview_theme_id=${THEME_ID}`,
      open: "local",
      https: {
        key: PATH_SSL_KEY,
        cert: PATH_SSL_CRT
      }
    },
    done
  );
});

gulp.task("dev", ["browsersync", "shopifywatch", "watch"]);
gulp.task("default", ["dev"]);

/* function need for run */
function makeLiquidSourceMappingURL(file) {
  return `{{"${file.relative}.map" | asset_url }}`;
}

function appendLiquidExt(path) {
  if (path.extname === ".map") return;
  if (path.extname === ".css") {
    path.extname = ".scss";
  }
  path.basename += path.extname;
  path.extname = ".liquid";
}

function reload(filename) {
  clearConsole();

  console.log("Reload file: " + filename);
  if (filename.search(/.map/g) !== -1) {
    return;
  } else if (filename.search(/scss\.liquid/g) !== -1) {
    browsersync.reload("*.css");
  } else if (filename.search(/js\.liquid/g) !== -1) {
    browsersync.reload("*.js");
  } else {
    browsersync.reload();
  }
}

makeAssetKey = function(filepath) {
  filepath = makePathRelative(filepath);

  return encodeURI(filepath);
};
makePathRelative = function(filepath) {
  var newBasePath = process.cwd() + "/src";
  filepath = path.relative(newBasePath, filepath);

  return filepath.replace(/\\/g, "/");
};
