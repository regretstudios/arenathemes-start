const task1 = () => {
  console.log("task1");
};

const task2 = () => {
  console.log("task2");
};

const task3 = () => {
  console.log("task3");
};

export { task2, task3 };

export default task1;
