require("dotenv").config();
const nameFileMergeJs = process.env.NAME_FILE_MERGE_JS || "arn-theme.js";

module.exports = {
  mode: process.env.NODE_ENV,
  // entry: "./local/scss/main.scss",

  output: {
    filename: nameFileMergeJs
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        loader: "babel-loader",
        query: {
          presets: ["@babel/env"]
        }
      }
    ]
  },

  devtool: "inline-source-map"
};
