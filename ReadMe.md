### Huong dan dung theme

chinh sua thong tin trong file .env

```md
APIKEY_PRIVATE_APP=api_key

PASSWORD_PRIVATE_APP=pass_key

SITE_URL=ten_web

THEME_ID=theme_id

BROWSER_SYNC_PORT= 3001

ACCESS_DEST=assets

NAME_FILE_MERGE_CSS=arn-theme.css

NAME_FILE_MERGE_JS=arn-theme.js

NODE_ENV=development
```

Tạo SSL:

# CLI: Https Option

By default webpack-dev-server will generate a self-signed, 2048 bit, sha256 SSL
Certificate, which is used to enable https. The certificate will be located in the
`ssl` directory after the server is started for the first time. The generated
certificate is only good for 30 days, at which point it'll be regenerated.

We highly recommend creating and managing your own certificates. Please see the
following resources for doing so:

### MacOS

https://certsimple.com/blog/localhost-ssl-fix

### Windows 10

https://technet.microsoft.com/itpro/powershell/windows/pkiclient/new-selfsignedcertificate

### Windows 7

https://msdn.microsoft.com/en-us/library/aa386968.aspx

Example (the .pfx file generated the following way can be used without `--pfx-passphrase`):

```
makecert -r -pe -sky exchange -sv makecert.pvk makecert.cer
pvk2pfx -pvk makecert.pvk -spc makecert.cer -pfx makecert.pfx
```

## Getting Started

```shell
npm run webpack-dev-server -- --open --https
```

## Using Your Certificate

Options are available for using your own SSL Certificate in your preferred or
OS-required format.

Given the base command `npm run webpack-dev-server -- --open --https`, append
one of the following:

- (PEM Files) `--cert=../../ssl/server.pem --key=../../ssl/server.pem`
- (PFX and Passphrase) `--pfx=./test_cert.pfx --pfx-passphrase=sample`

## What To Expect

The script should open `https://localhost:8080/`in your default browser. If your
browser displays a warning about a non-trusted certificate, follow the procedure
for your browser of choice to continue. After doing so you should see "It's Working"
displayed on the page.

Chay theme

````bash
npm install

gulp

```bash

chay thanh cong:
```bash
[Browsersync] Proxying: https://newshop46.myshopify.com
[Browsersync] Access URLs:
 ---------------------------------------------------------------------
       Local: https://localhost:3001/?preview_theme_id=38564069449
    External: https://192.168.4.118:3001/?preview_theme_id=38564069449
 ---------------------------------------------------------------------
          UI: http://localhost:3002
 UI External: http://192.168.4.118:3002
 ---------------------------------------------------------------------
[18:03:14] Finished 'browsersync' after 927 ms
[Browsersync] Couldn't open browser (if you are using BrowserSync in a headless environment, you might want to set the open option to false)
````
